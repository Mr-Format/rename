# rename

#### 介绍
Python批量重命名文件

```py
import os

path = 'D:\\example'
files = os.listdir(path)
index = 0
s = []

for file in files:
    last = '.' + file.split('.')[1]
    print(last)
    old_name = os.path.join(path, file)
    new_file = str(index) + last
    new_name = os.path.join(path, new_file)
    os.rename(old_name, new_name)
    index = index + 1
    print(index)

```